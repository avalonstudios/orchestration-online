<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin', 'cpts']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);

function videoIframe( $iframe ) {
    // Load value.
    // $iframe = get_field('oembed');

    // Use preg_match to find iframe src.
    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];

    // Add extra parameters to src and replcae HTML.
    $params = array(
        'controls'  => 0,
        'hd'        => 1,
        'autohide'  => 1
    );
    $new_src = add_query_arg($params, $src);
    $iframe = str_replace($src, $new_src, $iframe);

    // Add extra attributes to iframe HTML.
    $attributes = 'frameborder="0"';
    $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

    // Display customized HTML.
    echo $iframe;
}

function imgResize( $image, $width, $height = null, $crop = null, $single = true, $upscale = false ) {
    $img = $image[ 'url' ];
    $alt = $image[ 'alt' ];
    $img = aq_resize( $img, $width, $height, $crop, $single, $upscale );

    $output = "<img src='{$img}' alt='{$alt}'>";

    echo $output;
}

if ( isset( $_POST[ 'fe_pending' ] ) && $_POST[ 'fe_pending' ] == 'fe_pending' ) {
    if ( isset( $_POST[ 'pid' ] ) && !empty( $_POST[ 'pid' ] ) ) {
        change_post_status( (int) $_GET[ 'p' ], 'pending' );
    }
}

function change_post_status( $post_id, $status ) {
	$current_post = get_post( $post_id, 'ARRAY_A' );
    $current_post[ 'post_status' ] = $status;

    $options                        = get_fields( 'options' );
    $teacherFormThankYou            = $options[ 'page_links' ][ 'teacher_listing_thank_you' ];
    $websiteEmail                   = $options[ 'other' ][ 'contact_form_email' ];
    $teacherSubmissionConfirmation  = $options[ 'other' ][ 'user_submission_confirmation' ];

    $flds   = get_fields( $post_id );
	$name   = $flds[ 'name' ];
    // $email  = $flds[ 'email' ];
    $photo  = $flds[ 'photo' ];


    $headers =  [
                    "From:"         => "Orchestration Online <{$websiteEmail}>"
                    // "Reply-To:"     => $email
                ];
    // $attachment = get_attached_file( $photo[ 'ID' ] );

    $current_post[ 'post_title' ] = $name;
	$current_post[ 'post_name' ] = $name;
    wp_update_post( $current_post );

	// the message
    $msg = $teacherSubmissionConfirmation;
    $msg .= "<br><a href='" . home_url('/') . "wp-admin/post.php?post={$post_id}&action=edit'>Review Listing</a>";
	// use wordwrap() if lines are longer than 70 characters
    $msg = wordwrap( $msg, 70 );
    $subject = "You have a new Teach Listing submission.";
	// Make email HTML
    add_filter( 'wp_mail_content_type', 'my_custom_email_content_type' );
    // Send mail to OO
    $sendMail = wp_mail( "{$name} <{$websiteEmail}>", $subject, $msg, $headers );

    if ( $sendMail ) {
        wp_redirect( $teacherFormThankYou );
        exit;
    }
}

function my_custom_email_content_type() {
    return 'text/html';
}

if ( ! function_exists( 'ava_pagination' ) ) :
    function ava_pagination( $paged = '', $max_page = '' )
    {
        $big = 999999999; // need an unlikely integer
        if( ! $paged )
            $paged = get_query_var('paged');
        if( ! $max_page )
            $max_page = $wp_query->max_num_pages;

        $args = [
            'base'       => str_replace($big, '%#%', esc_url(get_pagenum_link( $big ))),
            'format'     => '?paged=%#%',
            'current'    => max( 1, $paged ),
            'total'      => $max_page,
            'mid_size'   => 1,
            'prev_text'  => __('«'),
            'next_text'  => __('»'),
            'type'       => 'list'
        ];

        echo '<div class="posts-pagination">';
        echo paginate_links( $args );
        echo '</div>';
    }
endif;
