export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    $(window).on('scroll', onScroll);

    $('.nav-primary li.menu-item-has-children .drop-dropdown').click(function (e) {
      e.preventDefault();

      if ($(window).innerWidth() > 1024) {
        return;
      }

      var $this = $(this);

      if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
      } else {
        $this.parent().parent().find('.sub-menu').removeClass('show');
        $this.parent().parent().find('.sub-menu').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
      }
    });

    let $open = $('.hamburger, .nav-primary, .footer-menu, .menu-underlay, .social-header, .login-register-menu-small');
    $('.footer-menu .close-menu').on('click', () => {
      $open.removeClass('open');
    });

    $('.hamburger').on('click', () => {
      $open.addClass('open');
    });

    $('.show-hide-details').on('click', function() {
      let $teacherDetails = $('.teacher-details');

      if ($(this).find('.svg-inline--fa').hasClass('fa-minus')) {
        $('.show-hide-details .svg-inline--fa').removeClass('fa-minus').addClass('fa-plus');
      } else {
        $('.show-hide-details .svg-inline--fa').removeClass('fa-minus').addClass('fa-plus');
        $(this).find('.svg-inline--fa').removeClass('fa-plus').addClass('fa-minus');
      }

      if ($(this).closest('article').find('.teacher-details').hasClass('open')) {
        $teacherDetails.slideUp().removeClass('open');
      } else {
        $teacherDetails.slideUp().removeClass('open');
        $(this).closest('article').find('.teacher-details').slideDown().addClass('open').css('display', 'grid');
      }
    });

    $('img').attr('loading', 'lazy');

    /**
     * Cookies
     */

    checkCookie();
    $('.nightmode').on('click', () => {
      let nightmode = checkCookie();
      if (nightmode == 0) {
        setCookie('nightmode', 1, 1);
        checkCookie();
      } else {
        setCookie('nightmode', 0, 1);
        checkCookie();
      }
    });
  },
};

function onScroll() {
  let offset = $(window).scrollTop();

  if (offset > 820) {
    $('#back-to-top').fadeIn();
  } else {
    $('#back-to-top').fadeOut();
  }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
  var name = cname + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

function checkCookie() {
  var nightmode = getCookie('nightmode');
  let html = document.getElementsByTagName('html')[0];
  if (nightmode == '') {
    setCookie('nightmode', 0, 1);
  }

  if (nightmode == 1) {
    $('.nightmode').addClass('nightmode-on');
    html.style.setProperty('--c_bodycolor', '#ddd');
    html.style.setProperty('--c_bodybackground', '#555');
    html.style.setProperty('--c_grey', '#444');
    html.style.setProperty('--c_offwhite', '#666');
    html.style.setProperty('--c_cream', '#444');
    html.style.setProperty('--c_widgettitle', '#ddd');
  } else {
    $('.nightmode').removeClass('nightmode-on');
    html.style.setProperty('--c_bodycolor', '');
    html.style.setProperty('--c_bodybackground', '');
    html.style.setProperty('--c_grey', '');
    html.style.setProperty('--c_offwhite', '');
    html.style.setProperty('--c_cream', '');
    html.style.setProperty('--c_widgettitle', '');
  }
  return nightmode;
}
