{{--
  Title: Latest Blog Post
  Description: Display the latest blog post
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$args = [
          'post_type'             => 'post',
          'posts_per_page'        => 1,
          'ignore_sticky_posts'   => true
        ];

$thePosts = get_posts( $args );
@endphp

@component( 'comps.blocks', $componentVars )
  @forelse ($thePosts as $ppost)
    @php
    $postID = $ppost->ID;

    if ( has_post_thumbnail( $postID ) ) {
      $thumb = get_the_post_thumbnail_url( $postID );
    } else {
      $thumb = App::getOptions()['defaults']['default_replacement_image'];
      $class = ' no-image';
    }

    $img                = aq_resize( $thumb, 300, 300, true, true, true );
    $link               = get_the_permalink( $postID );
    $content            = get_the_content( '', '', $postID );
    $content            = wp_strip_all_tags( $content, true );
    $content            = substr( $content, 0, 200 );
    $button[ 'url' ]    = $link;
    $button[ 'target' ] = '';
    $button[ 'title' ]  = 'read more';
    @endphp

    <article @php post_class( 'latest-blog' ) @endphp>
      <div class="post-thumbnail{{ $class ?? '' }}">
        <a href="{!! $link !!}"><img src="{{ $img }}" alt="{!! get_the_title( $postID ) !!}"></a>
      </div>
      <header>
        <h2 class="entry-title"><a href="{{ $link }}">{!! get_the_title( $postID ) !!}</a></h2>
      </header>
      <div class="entry-summary">
        <p>{{ $content }}...</p>
        @include('comps.btns.btn')
      </div>
    </article>
  @empty
    <p>No posts to show.</p>
  @endforelse
@endcomponent
