{{--
  Title: Announcements
  Description: Display Announcement
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$image  = $flds[ 'image' ];
$text   = wp_kses_post( $flds[ 'text' ] );
@endphp

@component( 'comps.blocks', $componentVars )
  @if ( $image )
    <div class="announcement-image">
      {{ imgResize( $image, 9999, 400 ) }}
    </div>
  @endif

  @if ( $text )
    <div class="announcement-text">{!! $text !!}</div>
  @endif
@endcomponent
