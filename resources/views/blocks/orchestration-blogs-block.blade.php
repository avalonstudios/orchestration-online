{{--
  Title: Orchestration / Instrumentation Blogs
  Description: Orchestration or instrumentation blogs list
  Category: ava_block_category
  Icon: admin-comments
  Keywords: blog, blogs, orchestration, instrumentation
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$mainText = wp_kses_post( $flds[ 'main_text' ] );
$blogList = $flds[ 'blog_list' ];
@endphp

@component( 'comps.blocks', $componentVars )
  <div class="main-text">{!! $mainText !!}</div>
  @foreach ( $blogList as $list )
    @php
    $image = $list[ 'image' ];
    $link = $list[ 'link' ];
    $text = wp_kses_post( $list[ 'text' ] );
    @endphp

    <div class="orchestration-blogs">
      <div class="orchestration-blog">
        @if ( $link )
          <div class="link">
            @include( 'comps.acf_link' )
          </div>
        @endif
        @if ( $image )
          <div class="image">
            {{ imgResize( $image, 100, 100, true ) }}
          </div>
        @endif
        @if ( $text )
          <div class="text">
            {!! $text !!}
          </div>
        @endif
      </div>
    </div>
    @include('partials.separator')
  @endforeach
@endcomponent
