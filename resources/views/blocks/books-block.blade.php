{{--
  Title: Books
  Description: Displays book list
  Category: ava_block_category
  Icon: admin-comments
  Keywords: book, books
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$text = $flds[ 'main_text' ];
$books = $flds[ 'books' ];
@endphp

@component( 'comps.blocks', $componentVars )
  @if ( $text )
    <div class="main-text">{!! $text !!}</div>
  @endif

  @php
  if ( $books ) {
    $args = [
      'post_type'   => 'book_cpt',
      'post__in'    => $books,
      'orderby'     => 'post__in'
    ];

    $related_qry = new WP_Query( $args );

    if ( $related_qry->have_posts() ) {
      while ( $related_qry->have_posts() ) : $related_qry->the_post(); @endphp
        @include('partials.books')
      @php
    endwhile;
    }
    wp_reset_query();
  }
  @endphp
@endcomponent
