{{--
  Title: Links List
  Description: Displays a list of links e.g. resources links
  Category: ava_block_category
  Icon: admin-comments
  Keywords: links, resources, link
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$title = $flds[ 'links_title' ];
$links = $flds[ 'links' ];
@endphp

@component( 'comps.blocks', $componentVars )
  <h4>{{ $title }}</h4>
  <div class="links-list">
    @foreach ($links as $link)
      @php
      $title   = $link[ 'link' ][ 'title' ];
      $url     = $link[ 'link' ][ 'url' ];
      $target  = $link[ 'link' ][ 'target' ];

      if ( $target ) {
        $targetAttr = 'target="_blank" rel="nofollow"';
      } else {
        $targetAttr = 'rel="bookmark"';
      }
      @endphp

      <div class="link"><a href="{{ $url }}" {{ $targetAttr }}>{{ $title }}@if ( $target ) <i class="fas fa-external-link-alt"></i> @endif</a></div>

    @endforeach
  </div>
@endcomponent
