{{--
  Title: Text + Image
  Description: Displays text on the left, image on the right
  Category: ava_block_category
  Icon: admin-comments
  Keywords: text, image
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'block_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$textImages = $flds[ 'text_images' ];
$mainText = $flds[ 'main_text' ];
@endphp

@component( 'comps.blocks', $componentVars )
  @if ( $mainText )
    <div class="main-text">{!! $mainText !!}</div>
  @endif

  @if ( $textImages )
    @foreach ($textImages as $ti)
      @php
      $images = $ti[ 'images' ];
      $text = wp_kses_post( $ti[ 'text' ] );
      $title = $ti[ 'title' ];

      if ( $images ) {
        $numOfImages = count( $images );
        if ( $numOfImages > 3 ) {
          $numOfImages = 3;
        }
      }
      @endphp

      <div class="text-image-wrapper images-{{ $numOfImages }}">
        <div class="block-content">
          <h3 class="title">{{ $title }}</h3>
          <div class="text">
            {!! $text !!}
          </div>
        </div>

        @if ( $images )
          <div class="images">
            @foreach ($images as $image)
              @php
              $img = $image[ 'image' ];
              $link = $image[ 'image_link' ];
              @endphp
              @if ( $link )
              <a href="{{ $link }}" rel="nofollow">
              @endif
                {{ imgResize( $img, 200 ) }}
              @if ( $link )
              </a>
              @endif
            @endforeach
          </div>
        @endif
      </div>
      @include('partials.separator')
    @endforeach
  @endif
@endcomponent
