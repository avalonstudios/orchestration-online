{{--
  Template Name: Events Page
--}}
@extends('layouts.app')

@php
$today = (int) date( 'U' );
$eDate = (int) date( 'U', $expiry_date );
@endphp

@section('content')
  @if ( $eDate < $today )
  <h2 class="event-expired">Expired</h2>
  @endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
