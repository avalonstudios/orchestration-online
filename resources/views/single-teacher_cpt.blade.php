@extends('layouts.app')

@php
$flds       = get_fields();
$photo      = $flds[ 'photo' ];
$name       = $flds[ 'name' ];
// $styles     = $flds[ 'style_of_orchestration_taught' ];
$subjects   = $flds[ 'additional_subjects' ];
$comments   = $flds[ 'additional_comments' ];
$langs      = $flds[ 'languages_spoken' ];
$qual       = $flds[ 'qualifications' ];
$position   = $flds[ 'current_professional_position' ];
$link_email = $flds[ 'link_or_email' ];
$contact    = $flds[ 'contact' ];

if ( isset( $_GET[ 'edit' ] ) && $_GET[ 'edit' ] == true ) {
  $editCheck = true;
} else {
  $editCheck = false;
}

if ( isset( $_GET[ 'p' ] ) ) {
  $postID = $_GET[ 'p' ];
} else {
  $postID = '';
}

$postStatus = $post->post_status;
$author = (int) $post->post_author;
$currentUser = get_current_user_id();

$teachersFormPage = App::getOptions()[ 'page_links' ][ 'teacher_listing_form_page' ];
$button[ 'url' ] = "{$teachersFormPage}?pst={$post->ID}&modify=true";
$button[ 'title' ] = 'go back';
$button[ 'target' ] = '';
@endphp

@section('content')
  <article @php post_class() @endphp>
    @include('partials.content-teacher_cpt')

    @if ( $editCheck && $postID && $author === $currentUser && $postStatus !== 'publish' )
      <div class="acf-form-buttons-wrapper">
        @include('comps.btns.btn')

        <form id="submit_for_review" name="front_end_publish" method="POST" action="">
          <input type="hidden" name="pid" id="pid" value="{{ $postID }}">
          <input type="hidden" name="fe_pending" id="fe_pending" value="fe_pending">
          <input class="btn btn-danger" type="submit" name="submit" id="submit" value="Submit for Review">
        </form>
      </div>
    @endif
  </article>
@endsection
