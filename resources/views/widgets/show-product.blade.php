@php
$showProduct = $flds[ 'product' ];
$currency = get_woocommerce_currency_symbol();
@endphp

{!! $args[ 'before_widget' ] !!}
{!! $args[ 'before_title' ] !!}
{!! $instance[ 'title' ] !!}
{!! $args[ 'after_title' ] !!}

@foreach ($showProduct as $prod)
  @php
  $product            = wc_get_product( $prod->ID );
  $title              = $prod->post_title;
  $regularPrice       = $product->get_regular_price();
  $salePrice          = $product->get_sale_price();
  $link               = get_the_permalink( $prod->ID );
  $image[ 'url' ]     = wp_get_attachment_url( get_post_thumbnail_id( $prod->ID ) );
  $image[ 'alt' ]     = $title;

  $button[ 'url' ]    = $link;
  $button[ 'target' ] = '';
  $button[ 'title' ]  = $title;
  @endphp
  <div class="product">
    <div class="product-thumb">
      <a href="{{ $link }}" rel="bookmark">{{ imgResize( $image, 300, 300, false ) }}</a>
    </div>
    <div class="product-title">
      <a href="{{ $link }}" rel="bookmark"><h3>{{ $title }}</h3></a>
    </div>
    <div class="product-prices">
      @if ( $regularPrice )
        <ins>{!! $currency !!}{{ $regularPrice }}</ins>
      @endif
      @if ( $salePrice )
        <del>{!! $currency !!}{{ $salePrice }}</del>
      @endif
    </div>
    @include('comps.btns.btn')
  </div>
@endforeach

{!! $args[ 'after_widget' ] !!}
