{{--
  Template Name: Form Page
--}}

@extends('layouts.forms')

@php
if ( isset( $_GET[ 'modify' ] ) && $_GET[ 'modify' ] == true ) {
  $userIsModifying = true;
} else {
  $userIsModifying = false;
}

if ( isset( $_GET[ 'pst' ] ) ) {
  $postID = $_GET[ 'pst' ];
}

$fieldGroupKey = get_field( '_field_group_key' );
$parentFieldGroups = get_field_object( $fieldGroupKey )[ 'parent' ];
$postType = acf_get_field_group( $parentFieldGroups )[ 'location' ][ 0 ][ 0 ][ 'value' ];
@endphp

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @php
    if ( $userIsModifying ) {
      $args = [
                'post_id'		=> absint( $postID ),
                'field_groups'	=> [ $parentFieldGroups ],
                //'post_title'	=> true,
                'return'		        => "%post_url%&edit=true",
                'html_submit_button'	=> '<input type="submit" class="btn btn-success p-3 ml-0 mt-3" value="%s" />',
                'submit_value'			=> 'Update'
              ];
      acf_form( $args );
    } else {
      $args = [
                'post_id'		          => 'new_post',
                'field_groups'	=> [ $parentFieldGroups ],
                'uploader'            => 'basic',
                // 'post_title'	      => true,
                'return'		        => "%post_url%&edit=true",
                'new_post'		        =>  [
                                            'post_type'		=> $postType,
                                            'post_status'	=> 'draft'
                                          ],
                'html_submit_button'	=> '<input type="submit" class="create-submit-btn btn btn-danger p-3 ml-0 mt-3" value="%s" />',
                'submit_value'			  => 'Preview'
                // 'update_message'      => print_r($_POST)['fields']['field_5eb6f4f650251']
              ];
      acf_form( $args );
    }
    @endphp
  @endwhile
@endsection
