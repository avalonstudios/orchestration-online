@php
if ( get_post_type() === 'patreon_cpt' ) {
  return;
}
@endphp

<div class="entry-meta">
  <div class="the-date">
    <i class="fas fa-calendar-day"></i>
    <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date('M jS, Y') }}</time>
  </div>
  <div class="the-author">
    <span class="byline author vcard"><i class="fas fa-user"></i>
      {{ __('', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
        {{ get_the_author() }}
      </a>
    </span>
  </div>
</div>
