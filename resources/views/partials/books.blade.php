@php
$postID       = get_the_ID();
$title        = get_the_title();
$content      = get_the_content();
$images       = get_field( 'images', $postID );

if ( $images ) {
  $numOfImages  = count( $images );

  if ( $numOfImages >= 3 ) {
    $numOfImages = 3;
  }
} else {
  $numOfImages = 0;
}
@endphp

<div class="book-wrapper thumbs-{{ $numOfImages }}">
  <h3 class="book-title">{!! $title !!}</h3>
  <div class="book-content">{!! $content !!}</div>

  @if ( $images )
    <div class="book-images">
      @foreach ($images as $image)
        @php
        $link = $image[ 'image_link' ];
        @endphp
        <div class="image">
          @if ( $link )
          <a href="{{ $link }}" rel="nofollow" target="_blank">
          @endif
            {{ imgResize( $image[ 'image' ], 200 ) }}
            @if ( $link )
          </a>
          @endif
          @if ( $link )
            <a class="buy-book-link" href="{{ $link }}" rel="nofollow" target="_blank">Buy book <i class="fas fa-external-link-alt"></i></a>
          @endif
        </div>
      @endforeach
    </div>
  @endif
</div>
<div class="separator thin-sep"></div>
