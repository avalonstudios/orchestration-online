@php
$flds = get_fields();
// $pageID = get_the_ID();

if ( is_front_page() || get_post_type() == 'teacher_cpt' ) {
  return;
} elseif ( is_shop() ) {
  $pageID = get_option( 'woocommerce_shop_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = 'SHOP';
} elseif ( is_cart() ) {
  $pageID = get_option( 'woocommerce_cart_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_checkout() ) {
  $pageID = get_option( 'woocommerce_checkout_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_home() ) {
  $pageID = get_option( 'page_for_posts' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_singular( 'pet_cpt' ) ) {
  $img = get_field( 'main_image' );
  $title = App::title();
} elseif ( is_product() ) {
  return;
  $img = get_the_post_thumbnail_url();
  $title = get_the_title();
} elseif ( is_single() ) {
  $pageID = get_the_ID();
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_archive() ) {
  // $pageID = get_the_ID();
  // $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} else {
  $pageID = get_the_ID();
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
}
if ( isset( $img ) ) {
  $img = aq_resize( $img, 1200, 404, true, true, true );
} else {
  $img = '';
}
@endphp

<div class="page-header">
  @if ( $img )
    <img src="{{ $img }}" alt="{!! App::title() !!}">
  @endif
  <h1>{!! $title !!}</h1>
</div>
