<header id="#top" class="banner">
  <div class="container">
    <div class="menu-grid">
      {!! App::getCustomLogo() !!}
      <div class="hamburger"><i class="fas fa-bars"></i></div>
      <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      @include('partials.menus.social-menu', [ 'place' => 'social-header' ])
    </div>
  </div>
</header>
@include('partials.menus.main-menu')
