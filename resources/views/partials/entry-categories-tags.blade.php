@php
if ( get_post_type() !== 'post' ) {
  return;
}

$cats = get_the_category();
$taxs = get_the_tags();
@endphp

<div class="entry-categories-taxonomies">
  <div class="entry-categories-taxonomies-title">
    <h3>Filed under</h3>
  </div>

  @if ( $cats )
    <div class="entry-categories">
      <div class="entry-categories-title">Categories</div>

      @foreach ($cats as $cat)
        @php
        $name = $cat->name;
        $link = get_term_link( $cat->term_id );
        @endphp
        <span class="entry-category">
          <a href="{{ $link }}" rel="bookmark">{{ $name }}</a>@if ( ! $loop->last ),@endif
        </span>
      @endforeach
    </div>
  @endif

  @if ( $taxs )
    <div class="entry-tags">
      <div class="entry-tags-title">Tags</div>

      @foreach ($taxs as $cat)
        @php
        $name = $cat->name;
        $link = get_term_link( $cat->term_id );
        @endphp
        <span class="entry-category">
          <a href="{{ $link }}" rel="bookmark">{{ $name }}</a>@if ( ! $loop->last ),@endif
        </span>
      @endforeach
    </div>
  @endif
</div>
