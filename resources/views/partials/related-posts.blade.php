@php
if ( get_post_type() === 'patreon_cpt' ) {
  return;
}
@endphp

<div class="related-posts">
  <h2 class="related-posts-title block-title">Related Posts</h2>
  <div class="related-posts-grid">
    @php
    $cats = wp_get_post_categories( $post->ID );

    if ( $cats ) {
      $innerArg[ 'category__in' ] = $cats;
      $innerArg[ 'post__not_in' ] = [ $post->ID ];
    } else {
      $innerArg[ 'post_type' ]    = 'post';
      $innerArg[ 'orderby' ]      = 'rand';
    }

    $args = [
      $innerArg,
      'posts_per_page'       => 8,
      'ignore_sticky_posts'  => 1
    ];

    $related_qry = new WP_Query( $args );

    if ( $related_qry->have_posts() ) {
      while ( $related_qry->have_posts() ) : $related_qry->the_post(); @endphp
        <div class="related-post">
          @include('partials.content-related-posts')
        </div>
      @php
    endwhile;
    }
    wp_reset_query();
    @endphp
  </div>
</div>
