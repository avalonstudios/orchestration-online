@php
if ( has_post_thumbnail() ) {
  $thumb = get_the_post_thumbnail_url();
} else {
  $thumb = App::getOptions()['defaults']['default_replacement_image'];
  $class = ' no-image';
}

$img = aq_resize( $thumb, 300, 300, true, true, true );
$link = get_the_permalink();
@endphp

<article @php post_class() @endphp>
  <div class="post-thumbnail{{ $class }}">
    <a href="{!! $link !!}"><img src="{{ $img }}" alt="{!! get_the_title( $postID ) !!}"></a>
  </div>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
</article>
