@php
if ( ! isset( $listingID ) ) {
  $listingID = get_the_ID();
}

$flds       = get_fields( $listingID );
$photo      = $flds[ 'photo' ];
$name       = $flds[ 'name' ];
$styles     = $flds[ 'style_of_orchestration_taught' ];
$subjects   = $flds[ 'additional_subjects' ];
$comments   = $flds[ 'additional_comments' ];
$langs      = $flds[ 'languages_spoken' ];
$qual       = $flds[ 'qualifications' ];
$position   = $flds[ 'current_professional_position' ];
$link_email = $flds[ 'link_or_email' ];
$contact    = $flds[ 'contact' ];

$link = get_the_permalink( $listingID );
@endphp

<article @php post_class( '', $listingID ) @endphp>
  <div class="teacher-meta">
    @if ( $photo )
    <div class="teacher-photo">{{ imgResize( $photo, 150, 150, false ) }}</div>
    @endif
    <h1 class="teacher-name">@if ( !is_single() )
        <a href="{{ $link }}" rel="bookmark">{{ $name }}</a>
      @else
        {{ $name }}
      @endif</h1>
    <div class="teacher-contact">
      @if ( $link_email == 'link' && $contact )
        <a href="{{ $contact }}" class="link" target="_blank" rel="nofollow">{{ $contact }}</a>
      @elseif ( $link_email == 'email' && $contact )
        <a href="mailto:{{ $contact }}" class="email" target="_blank" rel="nofollow">{{ $contact }}</a>
      @else
        <p>No contact details provided.</p>
      @endif
    </div>
    @if ( ! is_single() )
    <div class="show-hide-details"><i class="fas fa-plus"></i></div>
    @endif
  </div>
  <div class="teacher-details">
    <div class="styles-wrapper">
      <h3>Styles</h3>
      <ul class="styles">
        @foreach ( $styles as $style )
          @php
          $link = get_term_link( $style->term_id, 'style_taught' );
          $name = $style->name;
          @endphp
          <li><a href="{{ $link }}" rel="bookmark" title="List teachers by - {{ $name }}">{{ $name }}</a></li>
        @endforeach
      </ul>
    </div>
    @if ( $subjects )
      <div class="subjects-wrapper">
        <h3>Subjects</h3>
        <ul class="subjects">
          @foreach ( $subjects as $subject )
            @php
            $link = get_term_link( $subject->term_id, 'additional_subject' );
            $name = $subject->name;
            @endphp
            <li><a href="{{ $link }}" rel="bookmark" title="List teachers by - {{ $name }}">{{ $name }}</a></li>
          @endforeach
        </ul>
      </div>
    @endif
    @if ( $comments )
      <div class="comments">
        <h3>Comments</h3>
        {!! $comments !!}
      </div>
    @endif
    @if ( $langs )
      <div class="langs">
        <h3>Languages spoken</h3>
        {!! $langs !!}
      </div>
    @endif
    @if ( $qual )
      <div class="qual">
        <h3>Qualifications</h3>
        {!! $qual !!}
      </div>
    @endif
    @if ( $position )
      <div class="position">
        <h3>Current professional position</h3>
        {!! $position !!}
      </div>
    @endif
  </div>
</article>
