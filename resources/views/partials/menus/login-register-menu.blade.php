@php
$loginURL = App::login_register_menu()[ 'loginout' ];
$register = App::login_register_menu()[ 'register' ];
$cartURL = App::login_register_menu()[ 'cart_url' ];
$cartCount = WC()->cart->get_cart_contents_count();
@endphp

<nav class="login-register-menu">
  <ul class="login-navigation list-unstyled">
    <li class="loginout-menu-item">
      {!! $loginURL !!}
    </li>
    <li class="register-menu-item">
      <a href="{!! $register !!}">Register</a>
    </li>
    <li class="ava-cart-basket-icon{{ $cartCount > 0 ? ' cart-has-items' : '' }}">
      <a class="last-item" href="{{ $cartURL }}">
        <i class="fas fa-shopping-cart"></i>
        <sup class="cart-count">{{ $cartCount }}</sup>
      </a>
      {{-- @include( 'shop.cart-popup' ) --}}
    </li>
  </ul>
</nav>
