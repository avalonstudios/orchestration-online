<nav class="nav-primary">
  <div class="container">
    <div class="menus-wrapper">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu([
            'theme_location' => 'primary_navigation',
            'container_class' => 'main-menu',
            //'container_id' => '-container-id',
            //'menu_id' => 'ul-menu-id',
            'menu_class' => 'nav',
            'walker' => new MDBootstrapMenu(  )
          ])
        !!}
      @endif
      @include('partials.menus.login-register-menu')
    </div>
  </div>
  <div class="nightmode"><i class="far fa-eye"></i></div>
</nav>
