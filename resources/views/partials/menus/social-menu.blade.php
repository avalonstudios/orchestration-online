@php
$socialIcons = App::getOptions(  )[ 'social_icons' ][ 'social_icons' ];

if ( ! $socialIcons ) {
  return;
}
@endphp

<div class="social-menu {{ $place }}">
  <ul class="social-icons">
      @each( 'comps.social-icon-li-a', $socialIcons, 'sIcon' )
  </ul>
</div>
