<article @php post_class() @endphp>
  <header>
    @include('partials/entry-meta')
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    @include('partials/entry-categories-tags')
  </footer>
  @if ( get_post_type() !== 'patreon_cpt' )
    @include('partials.post-pagination')
  @endif
  @php comments_template('/partials/comments.blade.php') @endphp
  @include('partials.related-posts')
</article>
