<a href="#top" id="back-to-top"><i class="fas fa-angle-up"></i></a>
<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
  @include('partials.menus.social-menu', [ 'place' => 'social-footer' ])
  <div class="copyright-design">
    <div class="copyright"><a href="{{ get_bloginfo( 'url' ) }}" rel="home">Copyright &copy; {{ date( 'Y' ) }} Orchestration Online</a></div>
    <div class="sep">|</div>
    <div class="design"><a href="https://avalonstudios.eu" target="_blank" rel="nofollow">Design by Avalon Studios</a></div>
    <div class="sep">|</div>
    <div class="powered"><a href="https://cloud1500.com" target="_blank" rel="nofollow">Powered by Cloud1500</a></div>
  </div>
</footer>

<div class="footer-menu">
  <div class="close-menu"><i class="fas fa-times"></i></div>
  {!! App::getCustomLogo() !!}
  @include('partials.menus.main-menu')
  @include('partials.menus.social-menu', [ 'place' => 'social-mobile' ])
</div>
<div class="menu-underlay"></div>
