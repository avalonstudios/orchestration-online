@php
if ( ! is_single() ) {
  return;
}

$nextPost = get_next_post();
if ( $nextPost ) {
  $nextTitle = wp_kses_post( $nextPost->post_title );
  $nextLink = get_the_permalink( $nextPost->ID );
}

$prevPost = get_previous_post();
if ( $prevPost ) {
  $prevTitle = wp_kses_post( $prevPost->post_title );
  $prevLink = get_the_permalink( $prevPost->ID );
}
@endphp



<div class="post-navigation">
  @if ( get_previous_post_link() )
    <span class="prev-link">
      <a href="{{ $prevLink }}" rel="prev">&laquo; {!! $prevTitle !!}</a>
    </span>
  @endif

  @if ( get_next_post_link() )
    <span class="next-link">
      <a href="{{ $nextLink }}" rel="next">{!! $nextTitle !!} &raquo;</a>
    </span>
  @endif
</div>
