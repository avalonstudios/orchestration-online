@php
$fullWidth = get_field( 'full_width' );

if ( ! $fullWidth ) {
  $class = ' has-sidebar';
}
@endphp

<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php acf_form_head() @endphp
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      @include('partials.page-header')
      <div class="content{{ $class ?? '' }}">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
