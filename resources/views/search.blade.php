@extends('layouts.app')

@section('content')
  @if ( isset( $_GET[ 'k' ] ) )
    @php
    $args = [
              'numberposts'	=>  -1,
              'post_type'		=>  [ 'teacher_cpt' ],
              'meta_query'	=>  [
                                  [
                                    'key'	  	  => $_GET[ 'k' ],
                                    'value'	  	=> '"' . get_search_query() . '"',
                                    'compare' 	=> 'LIKE',
                                  ],
                                ],
            ];
    $teacherPosts = get_posts( $args );
    @endphp

    @forelse ($teacherPosts as $listing)
      @php
      $listingID = $listing->ID;
      @endphp
      @include('partials.content-teacher_cpt')
    @empty
      <p>Nothing to show</p>
    @endforelse
  @else

    @if (!have_posts())
      <div class="alert alert-warning">
        {{ __('Sorry, no results were found.', 'sage') }}
      </div>
      {!! get_search_form(false) !!}
    @endif

    @while(have_posts()) @php the_post() @endphp
      @include('partials.content')
    @endwhile
  @endif

  {!! ava_custom_pagination() !!}
@endsection
