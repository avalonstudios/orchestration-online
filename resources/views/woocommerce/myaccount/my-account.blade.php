{{--
My Account page

This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see     https://docs.woocommerce.com/document/template-structure/
@package WooCommerce/Templates
@version 3.5.0
--}}

@php

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); @endphp

<div class="woocommerce-MyAccount-content">
	@php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
    do_action( 'woocommerce_account_content' );

    $currentUser = get_current_user_id();

    $args = [
      'post_type'       => [ 'teacher_cpt' ],
      'author'          => $currentUser,
      'posts_per_page'  => -1,
      'post_status'     => [ 'publish', 'draft', 'pending' ],
    ];

    $currentUserTeacherListings = get_posts( $args );
    $teachersFormPage = App::getOptions()[ 'page_links' ][ 'teacher_listing_form_page' ];
  @endphp

  <table id="abandonedListings" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="th-sm">Title</th>
        {{-- <th class="th-sm">Listing Type</th> --}}
        <th class="th-sm">Status</th>
      </tr>
    </thead>
    <tbody>
      @foreach ( $currentUserTeacherListings as $listing )
        <tr>
          <td>
            @php
            $title = $listing->post_title;
            $link = "{$teachersFormPage}?pst={$listing->ID}&modify=true";
            $postStatus = $listing->post_status;
            $publishedLink = get_the_permalink( $listing->ID );
            /* $postType = get_post_type_object( 'teacher_cpt' );
            $postType = $postType->labels->singular_name; */
            @endphp
            @if ( $postStatus === 'publish' )
              <a class="view-listing" href="{{ $publishedLink }}">{{ $title }} (view)</a>
            @else
              <a class="edit-listing" href="{{ $link }}">{{ $title }} (edit)</a>
            @endif
          </td>
          {{-- <td class="text-capitalize">{{ $postType }}</td> --}}
          <td class="text-capitalize">{{ $listing->post_status }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

</div>
