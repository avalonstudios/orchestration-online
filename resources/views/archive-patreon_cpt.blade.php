@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
  @endif

  @if (have_posts())
    <ul>
      @while (have_posts()) @php the_post() @endphp
        <li><a href="{{ the_permalink() }}" rel="bookmark">{{ the_title() }}</a></li>
      @endwhile
    </ul>
  @endif

  {!! ava_custom_pagination() !!}
@endsection
