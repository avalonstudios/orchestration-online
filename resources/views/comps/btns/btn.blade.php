@php
if ( ! $button ) {
  return;
}
$url     = $button[ 'url' ];
$target  = $button[ 'target' ];
$title   = $button[ 'title' ];
@endphp

<div class="btn-wrap{{ $wrapClass ?? '' }} wrap-{{ $type ?? 'primary' }}">
  <a href="{{ $url }}" class="btn btn-{{ $type ?? 'primary' }}{{ $classes ?? '' }}"{{ $target ? ' target="_blank" rel="nofollow"' : ' rel="bookmark"' }}>{{ $title ?? 'more' }}<span class="cursor"></span></a>
</div>
