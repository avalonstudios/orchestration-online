@php
$title   = $link[ 'title' ];
$url     = $link[ 'url' ];
$target  = $link[ 'target' ];

if ( $target ) {
  $targetAttr = 'target="_blank" rel="nofollow"';
} else {
  $targetAttr = 'rel="bookmark"';
}
@endphp

<a href="{{ $url }}" {{ $targetAttr }}>{{ $title }}@if ( $target ) <i class="fas fa-external-link-alt"></i> @endif</a>
