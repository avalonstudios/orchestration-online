<section
  data-{{ $id }}
  id="{{ $blockID }}"
  class="ava-block {{ $classes }}{{ $other_classes }}"
  @if ( $backImg ) style="background-image: url( '{{ $backImg }}' );" @endif
  >

  <div class="ava-block-wrapper {{ $slug }}-wrapper">
    @if ( $title )
      <h2 class="block-title">{{ $title }}</h2>
    @endif
    <div class="ava-block-content">
      {{ $slot }}
    </div>
  </div>
  @include('partials.block-styles')
</section><!-- {{ $slug }} -->
