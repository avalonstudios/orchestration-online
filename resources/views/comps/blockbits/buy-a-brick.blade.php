@php
$donationsCount = count( $donations );
$i = 1;
@endphp

@foreach ( $donations as $donation )
  @php
  $last_name = ''; // clear last name with every iteration

  $hasSurname = strpos( $donation, " " );
  $hasAmount = strpos( $donation, "|" );

  $amount = '';
  $firstName = '';
  $lastName = '';

  if ( ! $hasAmount ) {
    if ( ! $hasSurname ) {
      $firstName = $donation;
    } else {
      $fullName = explode( " ", $donation );
      $firstName = $fullName[0];
      $lastName = $fullName[1];
    }
  } else { // we now have amount
    if ( ! $hasSurname ) {
      $nameAmount = explode( "|", $donation );
      $firstName = $nameAmount[0];
      $amount = $nameAmount[1];
    } else {
      $nameAmount = explode( "|", $donation );
      $fullName = explode( " ", $nameAmount[0] );
      $firstName = $fullName[0];
      $lastName = $fullName[1];
      $amount = $nameAmount[1];
    }
  }
  @endphp
  <div class="square donation-{{ $amount ?? '' }}">
  <span class="name">{{ $firstName }}{{ $lastName ? ' ' . $lastName[0] . '.' : '' }}</span>
    <div class="squar square-popup donation-{{ $amount ?? '' }}">
      <span>{{ $firstName }} {{ $lastName ?? '' }}</span>
      @if ( $amount )
      <span>&euro;{{ $amount ?? '' }}</span>
      @endif
    </div>
  </div>
@endforeach
@while ( $i <= ( $acceptedDonations - $donationsCount ) )
  <div class="square donation-empty">{{ $i + $donationsCount }}</div>
  @php
  $i++;
  @endphp
@endwhile

