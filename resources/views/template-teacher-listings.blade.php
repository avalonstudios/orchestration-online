{{--
  Template Name: Teacher Listings
--}}

@php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = [
  'post_type' => 'teacher_cpt',
  'paged'     => $paged
];
$listingsQry = new WP_Query( $args );
@endphp

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile

  @each('partials.content-teacher_cpt', $listingsQry->posts, 'listingID')

  {!! ava_pagination($paged, $listingsQry->max_num_pages) !!}

  @php
    wp_reset_postdata();
  @endphp
@endsection
