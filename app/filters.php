<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return '&hellip; <br><a href="' . get_permalink() . '">' . __('Read more&hellip;', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Change URL for Login Logo
 */
add_filter( 'login_headerurl', function( $url ) {
    return get_home_url( );
} );

/**
 * Add Login / Logout to main menu
 */
/* add_filter('wp_nav_menu_items', function( $items, $args ) {
	ob_start();
	wp_loginout('index.php');
	$loginoutlink = ob_get_contents();
	ob_end_clean();
	$items .= '<li>'. $loginoutlink .'</li>';
	return $items;
}, 10, 2 ); */

add_filter( 'sage/display_sidebar', function ( $display ) {
    $fullWidth = get_field( 'full_width' );

    if ( $fullWidth ) {
        return;
    }

    static $display;

    $display = true;

    /* isset($display) || $display = in_array(true, [
        // The sidebar will be displayed if any of the following return true
        is_front_page(),
        is_home(),
        is_page(),
        is_404(),
        is_page_template('template-custom.php'),
        is_shop(),
        is_author(),
        is_single(),
        is_tag(),
        is_category(),
        is_search()
    ]); */

    // var_dump($display);

    return $display;
});

/* add_filter( 'acf/load_field/name=style_of_orchestration_taught', function( $field ) {
    $field[ 'choices' ] = [];

    $args = [
        'taxonomy' => 'style_taught',
        'hide_empty' => false
    ];

    $styles = get_terms($args);

    foreach ( $styles as $style ) {
        $value = $style->slug;
        $label = $style->name;

        $field[ 'choices' ][ $value ] = $label;
    }

    return $field;

} ); */
