<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateEventsPage extends Controller
{
    public function expiryDate()
    {
        $eD = get_field( 'expiry_date' );

        return $eD;
    }
}
