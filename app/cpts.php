<?php
add_action( 'init', function() {

    # These settings are in common for the 3 hotels
    $settings =     [
                        # Add the post type to the site's main RSS feed:
                        'show_in_feed'      =>  true,
                        'show_in_menu'      =>  'oo_menu',
                        'map_meta_cap'      =>  true,
                        'show_in_rest'      =>  false,
                        'hierarchical'      =>  true,
                    ];

    $icon =         [
                        'menu_icon' => 'dashicons-book-alt',
                    ];

    $capability =   [ 'capability_type' => 'page' ];

    $supports =     [
                        'supports' =>   [
                                            'title',
                                            'editor'
                                        ],
                    ];

    # $names, change for each hotel
    $names =        [
                        # Override the base names used for labels:
                        'singular'          => 'Book',
                        'plural'            => 'Books',
                        'slug'              => 'book'
                    ];

    # register cornucopia post type
    register_extended_post_type( 'book_cpt', $settings + $capability + $supports, $names );

    # $names, change for each hotel
    $names =        [
                        # Override the base names used for labels:
                        'singular'          => 'Patreon',
                        'plural'            => 'Patreon',
                        'slug'              => 'patreon-content'
                    ];

    $supports =     [
                        'supports' =>   [
                                            'title',
                                            'editor',
                                            'thumbnail'
                                        ],
                    ];

    # register cornucopia post type
    register_extended_post_type( 'patreon_cpt', $settings + $capability + $supports, $names );

    $tax_settings =     [
                            'taxonomies'    =>  [
                                                    'style_taught',
                                                    'additional_subject'
                                                ]
                        ];

    # $names, change for each hotel
    $names =        [
                        # Override the base names used for labels:
                        'singular'          => 'Teacher Listing',
                        'plural'            => 'Teacher Listings',
                        'slug'              => 'teacher-listing'
                    ];

    $capability =   [ 'capability_type' => 'post' ];

    $supports =     [
                        'supports' =>   [
                                            'title',
                                            'author'
                                        ],
                    ];

    # register cornucopia post type
    register_extended_post_type( 'teacher_cpt', $settings + $tax_settings + $capability + $supports, $names );

    /**
    * TAXONOMIES
    */

    # settings for taxonomies
	$settings =     [
                        # Use radio buttons in the meta box for this taxonomy on the post editing screen:
                        // 'meta_box'		    =>	'radio'
                        'capabilities'      =>  [
                                                    'assign_terms' => 'edit_others_posts'
                                                ]
                    ];

	# names for Pet Type (dog, cat, etc) taxonomy
	$names =	    [
                        # Override the base names used for labels:
                        'singular'			=> 'Style Taught',
                        'plural'			=> 'Styles Taught',
                        'slug'				=> 'style-taught'
                    ];

    register_extended_taxonomy( 'style_taught', 'teacher_cpt', $settings, $names );

    $names =	    [
                        # Override the base names used for labels:
                        'singular'			=> 'Additional Subject',
                        'plural'			=> 'Additional Subjects',
                        'slug'				=> 'additional-subject'
                    ];

    register_extended_taxonomy( 'additional_subject', 'teacher_cpt', $settings, $names );
} );


/** Register custom menu page. */
add_action( 'admin_menu', function() {
	add_menu_page(
		'Custom Posts',
		'Custom Posts',
		'edit_posts',
		'oo_menu',
		'',
		'dashicons-admin-home',
		6
    );

    add_submenu_page(
        'oo_menu',
        'Styles Taught',
        'Styles Taught',
        'edit_others_posts',
        'edit-tags.php?taxonomy=style_taught&post_type=teacher_cpt'
    );

    add_submenu_page(
        'oo_menu',
        'Additional Subjects',
        'Additional Subjects',
        'edit_others_posts',
        'edit-tags.php?taxonomy=additional_subject&post_type=teacher_cpt'
    );
} );

//http://orcon.test/wp-admin/
